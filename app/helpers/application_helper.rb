module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = 'Fungus Among Us')
    if page_title.empty?
      base_title = "Among Us is Fungus"
    else
      page_title + " | " + base_title
    end
  end
end
